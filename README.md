# my-stock-workshop

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

// npm install --save material-design-icons-iconfont
// import 'material-design-icons-iconfont/dist/material-design-icons.css'

# fronend

# vuejs

npm i axios chart.js material-design-icons-iconfont vue-chartjs vue-moment

# backend

# nodejs

npm i bcryptjs body-parser cors express formidable fs-extra jsonwebtoken moment sequelize sqlite3

yarn serve
yarn run build

#deployment
/usr/local/Cellar/nginx
vue.config.js

baseUrl: process.env.NODE_ENV === 'production'
? '/production-sub-path/'
: '/'

./mongod --dbpath /Users/narin/Desktop/mongo_db_files
http://travistidwell.com/jsencrypt/demo/

# Deployment

# Fallback

# location / {

        #    root   html;
        #    index  index.html index.htm;
        # }

        #no sub-folder
        location / {
            try_files $uri $uri/ /index.html;
        }

        # have sub-folder named "demo"
        location /demo {
          try_files $uri $uri/ /demo/index.html;
        }

#https://material.io/resources/icons/?style=outline
